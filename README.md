# csharp-tricks
Some tricky syntax for C#

## Singleton propagation

How to declare singleton in one line (+1 for the private field) with a null propagation expression.

```cs
private MyObject _singleton;

public MyObject Singleton => _singleton ?? (_singleton = new MyObject());
```
## Traits simulation

To fill.

## Arbitrary / Structured polymorphic value type

This construction allow us to define a set of structured values like an enum, but it have the advantage to be open to extension by adding arbitraries values. This is why it's called arbitrary and structured polymorphic value type.

It can be useful too for simulating string (or other type) enumeration.

```cs
public struct MyType
{
  //Arbitrary value
  private readonly string _value;

  private MyType(string value)
  {
    _value = value;
  }

  public static MyType From(string value) => 
      new MyType(value);

  //Structured values
  public static readonly MyType Hello = "hello";
  public static readonly MyType World = "world";

  public override string ToString() => _value;

  //Implicit conversions
  public static implicit operator string(MyType cv) => cv._value;
  public static implicit operator MyType(string v) => MyType.From(v);
}
```

using like this

```cs
void Main(string[] args)
{
  Test(MyType.Hello);
  Test(MyType.World);
  Test("arbitrary value");
}

void Test(MyType t) =>
  Console.Log(t);

```
